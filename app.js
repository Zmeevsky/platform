'use strict';

const
  express             = require('express'),
  app = express(),
  nunjucks            = require('nunjucks'),
  mongoose            = require('mongoose'),
  path                = require('path'),
  cfg                 = require('config'),
  favicon             = require('serve-favicon'),
  logger              = require('morgan'),
  cookieParser        = require('cookie-parser'),
  bodyParser          = require('body-parser'),
  session             = require('express-session'),
  RedisStore          = require('connect-redis')(session),
  redis               = require('redis'),
  redisCli            = redis.createClient(),
  passport            = require('passport'),
  LocalStrategy       = require('passport-local').Strategy,
  router              = require('./routes'),
  HttpError           = require('lib/HttpError'),
  passAuth            = require('lib/passAuth');


require('lib/logTactic')(app.get('env') === 'development');
require('lib/dbConnection')();
require('./models');


/* настройка модуля авторизации (локальная стратегия) */
passport.serializeUser(passAuth.serialize);
passport.deserializeUser(passAuth.deserialize);
passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, passAuth.checkUserData));

nunjucks.configure('views', {// настройка шаблонизатора
  autoescape: true,
  express: app
});

app /* базовая настройка */
  .set('trust proxy', 1)
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'html')
  .set('view cache', true)
  .use(favicon(path.join(__dirname, 'public/img', 'favicon.ico')))
  .use(express.static(path.join(__dirname, 'public')))
  .use(logger('dev'))
  .use(require('middleware/sendHttpError'))
  .use(cookieParser())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: false}));

app /* сессии, авторизация */
  .use(session({
    secret: cfg.get('session.secret'),
    name: cfg.get('session.name'),
    saveUninitialized: cfg.get('session.saveUninitialized'),
    resave: cfg.get('session.resave'),
    cookie: cfg.get('session.cookie'),
    store: new RedisStore({
      host: cfg.get('redis.host'),
      port: cfg.get('redis.port'),
      client: redisCli
    })
  }))
  .use(passport.initialize())
  .use(passport.session());

app /* роутинг, обработка ошибок */
  .use('/', router)
  .use(express.static(path.join(__dirname, 'storage')))
  .use((req, res, next) => { // обработка отсутствующей страницы
    let err = new HttpError(404, 'Страница не найдена');
    next(err);
  })
  .use((err, req, res, next) => {
    console.error('Ошибка:\n', err);
    if (typeof err === 'number') {
      err = new HttpError(err);
    }
    if (err instanceof HttpError) {
      res.sendHttpError(err);
    } else {
      if (app.get('env') === 'development') {
        res.sendHttpError(err);
      } else {
        err = new HttpError(500);
        res.sendHttpError(err);
      }
    }
  });


module.exports = app;