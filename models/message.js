'use strict';
const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let MessageSchema = new Schema({
  room: {
    type: ObjectId,
    ref: 'Room'
  },
  from: {
    id: {
      type: ObjectId,
      ref: 'User'
    },
    fullName: String
  },
  text: String,
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Message', MessageSchema);