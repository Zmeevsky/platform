'use strict';
const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let RoomSchema = new Schema({
  /**
   * 0 - main,
   * 1 - public,
   * 2 - private
   * */
  type: {
    type: Number,
    default: 1,
  },
  name: {
    type: String,
    default: randomName,
  },
  creator: {
    type: ObjectId,
    ref: 'User',
    require: false
  },
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Room', RoomSchema);

function randomName() {
  return Math.random().toString(36).slice(2, 10) + ' room';
}