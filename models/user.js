'use strict';
const
  cfg = require('config'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  crypto = require('crypto');

let UserSchema = new Schema({
  // todo соцсети?
  firstName: {
    type: String,
    trim: true,
    default: ''
  },
  lastName: {
    type: String,
    trim: true,
    default: ''
  },
  email: {
    type: String,
    //unique: true,
    lowercase: true,
    trim: true,
    default: '',
    validate: [
      {
        validator: value => {
          return value.length > 0;
        },
        message: 'Адрес электронной почты не может быть пустым.'
      },
      {
        validator: value => {
          let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(value);
        },
        message: 'Введите корректный адрес электронной почты.'
      }
    ]
  },
  salt: {
    type: String
  },
  passwordHash:  {
    type: String
  },
  created: {
    type: Date,
    default: Date.now
  }
});

UserSchema.path('email').validate(function(value, done) {
  let User = mongoose.model('User'); // this не работает с Update Validators
  User.count({ email: value }, (err, count) => {
    if (err) return done(err);
    done(!count);
  });
}, 'Адрес электронной почты уже используется.');

UserSchema // fixme виртуальные поля не работают с update
  .virtual('password')
  .set(function(password) {
    console.log('Генерируем хэш для: ', password);
    if (password !== undefined) {
      if (password.length < 4) {
        this.invalidate('password', 'Минимальная длина пароля 4 символа.');
      }
      if (password.length > 40) {
        this.invalidate('password', 'Максимальная длина пароля 40 символов.');
      }
    }

    this._plainPassword = password;

    if (password) {
      this.salt = hash.createSalt();
      this.passwordHash = hash.createHash(password, this.salt);
    } else { 
      this.salt = undefined;
      this.passwordHash = undefined;
    }
    console.log('Соль: %s \nХэш: %s', this.salt, this.passwordHash);
  })  
  .get(function() {
    console.log('Внутри геттера');
    return this._plainPassword;
  });

UserSchema.methods.checkPassword = function(password) {
  if (!password) return false;
  if (!this.passwordHash) return false;

  return hash.createHash(password, this.salt) === this.passwordHash;
};

UserSchema.methods.getFullName = function() {
  return `${this.firstName} ${this.lastName}`;
};



/** служебные */

var hash = {
  createSalt: () => {
    return crypto.randomBytes(cfg.get('hash.length')).toString('base64');
  },
  createHash: (password, salt) => {
    console.log('Password: %s \n Salt: %s', password, salt);
    return crypto.pbkdf2Sync(password, salt, cfg.get('hash.iterations'), cfg.get('hash.length'), 'sha512').toString('base64');
  }
};

mongoose.model('User', UserSchema);