'use strict';

module.exports = (req, res, next) => {
  res.sendHttpError = (err) => {
    let status = err.status;
    let error = err;

    if (err.name === 'ValidationError') { // обработка ошибок валидации мангуса
      error = [];
      status = 400;
      for (let key in err.errors) {
        if (err.errors.hasOwnProperty(key)) {
          error.push(err.errors[key])
        }
      }
    }

    res.status(status);

    if (res.req.headers['x-requested-with'] === 'XMLHttpRequest') { // если запрос был аяксом
      res.json(error);
    } else {
      res.render('error', {
        error: err,
        user: req.user
      });
    }
  };

  next();
};