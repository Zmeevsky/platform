'use strict';

const
  mongoose = require('mongoose'),
  HttpError = require('lib/HttpError');


module.exports = {
  getAllUsers: getAllUsers,
  getUserById: getUserById,
  updateUserData: updateUserData
};

function getAllUsers(req, res, next) {
  let User = mongoose.model('User');

  User.find((err, users) => {
    if (err) return next(err);
    res.json(users)
  });
}

function getUserById(req, res, next) {
  let id = req.params.id;
  getUserDataById(id, (err, user) => {
    if (err) return next(err);
    res.json(user);
  });
}

function updateUserData(req, res, next) {
  let id = String(req.user._id);
  let data = req.body;
  let User = mongoose.model('User');
  let newData = {
    firstName: data.first_name,
    lastName: data.last_name,
    email: data.email,
    password: data.password
  };

  for (let key in newData) {
    if (newData[key] === undefined) {
      delete newData[key];
    }
  }
  console.log('newData', newData);
  if (!Object.keys(newData).length) return next(new HttpError(400));


  User.findOneAndUpdate({_id: id}, newData, {new: true, runValidators: true}, (err, user) => {
    if(err) return next(err);

    req.logIn(user, err => {
      if (err) return next(err);
      //res.redirect(302, '/');
      res.send();
    });
  });
}

function getUserDataById(id, cb) {
  let User = mongoose.model('User');

  if(!mongoose.Types.ObjectId.isValid(id)) {
    return cb(400);
  }

  User.findById(id, (err, user) => {
    if (err) return cb(err);
    if (!user) return cb(new HttpError(404, 'Пользователь не найден.'));
    return cb(null, user);
  })
}