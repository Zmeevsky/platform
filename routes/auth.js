'use strict';

const
  passport = require('passport'),
  HttpError = require('lib/HttpError'),
  cookie = require('lib/cookie'),
  mongoose = require('mongoose');

module.exports = {
  isAuth: isAuth,
  login: login,
  logout: logout,
  join: join
};

function isAuth(req, res, next) {
  req.isAuthenticated() ? next() :
    res.req.headers['x-requested-with'] === 'XMLHttpRequest' ? next(new HttpError(403)) :
      res.redirect(302, '/');
}

function login(req, res, next) {
  console.log(req.body);

  passport.authenticate('local', (err, user, info) => {
    if (err) return next(err);
    if (!user) return next(new HttpError(400, info.message));

    req.logIn(user, err => {
      if (err) return next(err);
      cookie.set(req, res, 'pl-uid', req.user._id);
      res.send();
    });
    
  })(req, res, next);
}

function logout(req, res, next) {
  console.log('LOGOUT');
  req.logout();
  cookie.delAll(req, res);
  res.redirect(302, '/');
}

function join(req, res, next) {
  let
    data = req.body,
    User = mongoose.model('User');

  console.log(data);

  let user = new User({
    firstName: data.first_name,
    lastName: data.last_name,
    email: data.email,
    password: data.password
  });

  user.save((err, user) => {
    if (err) return next(err);
    req.logIn(user, err => {
      if (err) return next(new HttpError(err));
      cookie.set(req, res, 'pl-uid', req.user._id);
      res.send();
    });
  });
}