'use strict';
const
  router    = require('express').Router(),
  multer    = require('multer'),
  upload    = multer({dest: './uploads'}),
  auth      = require('./auth'),
  pages     = require('./pages'),
  users     = require('./users'),
  imgSt     = require('./imagesStorage');

module.exports = router;

/** авторизация */
router.post('/login', auth.login);
router.get('/logout', auth.logout);
router.post('/join', auth.join);

/** отдача страниц */
/* для всех */
router.get('/', pages.index);
router.get('/login', pages.login);
router.get('/join', pages.join);
/* для авторизованных пользователей */
router.get('/chat', auth.isAuth, pages.chat);
router.get('/gallery', auth.isAuth, pages.gallery);
router.get('/profile', auth.isAuth, pages.profile);

/** работа с пользователями */
// router.get('/users', users.getAllUsers);
// router.get('/user/:id', users.getUserById);

/** профиль игрока */
router.post('/update-user-data', auth.isAuth, users.updateUserData);

/** работа с хранилищем изображений */
router.get('/:id/private/:img', imgSt.accessCheck);
router.get('/gallery/get-all', auth.isAuth, imgSt.getAllImgs);
router.post('/uploads/multi', auth.isAuth, upload.any(), imgSt.multiUpload);
router.post('/gallery/img-delete', auth.isAuth, imgSt.deleteImg);
router.post('/gallery/toggle-access', auth.isAuth, imgSt.toggleAccess);
