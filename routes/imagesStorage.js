'use strict';

const
  fs = require('fs'),
  path = require('path'),
  async = require('async'),
  cfg = require('config'),
  HttpError = require('lib/HttpError');

module.exports = {
  accessCheck: accessCheck,
  getAllImgs: getAllImgs,
  multiUpload: multiUpload,
  deleteImg: deleteImg,
  toggleAccess: toggleAccess
};

/* отдача приватных файлов */
function accessCheck(req, res, next) {
  req.user && req.params.id === String(req.user._id) ? next() : next(new HttpError(403));
}

/* получить все файлы пользователя */
function getAllImgs(req, res, next) {
  let
    id = String(req.user._id),
    storage = path.join('./storage', id),
    privateDir = path.join(storage, 'private'),
    publicDir = path.join(storage, 'public'),
    filesList = [], re = /__(.*)\./;

  fs.stat(storage, (err, stats) => {
    if ((err && err.code === 'ENOENT') || !stats.isDirectory()) {
      res.send([]);
    } else if (err) {
      return next(err);
    } else {
      fs.readdir(privateDir, (err, list) => {
        if (err) return next(err);

        list.forEach((file) => {
          filesList.push({
            originalName: file.split('__').shift(),
            filename: file,
            created: file.match(re)[1],
            path: path.join(id, 'private', file)
          });
        });

        fs.readdir(publicDir, (err, list) => {
          if (err) return next(err);

          list.forEach((file) => {
            filesList.push({
              originalName: file.split('__').shift(),
              filename: file,
              created: file.match(re)[1],
              path: path.join(id, 'public', file)
            });
          });

          res.json(filesList);
        });
      });
    }
  });

}

/* мультизагрузчик изображений */
function multiUpload(req, res, next) {
  let 
    storage = path.join('./storage', String(req.user._id)),
    files = fileFilter(req.files),
    calls = [];

  if (!files.length) return next(new HttpError(400));

  createPersonalStorage(storage, (err) => {
    if (err) {
      console.error(err);
      return next(err);
    }
    
    files.forEach(file => {
      calls.push(function(cb) {
        saveFile(storage, file, cb);
      });
    });
    
    async.parallel(calls, (err, result) => {
      if (err) return next(err);
      console.log(result);
      
      res.json(result);
    });
    
  });

}

/* сохраняем файл, удалив копию из временной директории */
function saveFile(storagePath, file, cb) {
  let
    date = new Date(),
    tmp = file.path,
    originalName = file.originalname,
    created = `${date.toLocaleDateString()}_${date.toLocaleTimeString()}`,
    filename = `${originalName}__${created}${path.extname(originalName)}`,
    targetFile = path.join(storagePath, 'private', filename);
  
  moveFile(tmp, targetFile, (err) => {
    if (err) return cb(err);
    return cb(null, {
      originalName: originalName,
      filename: filename,
      created: created,
      path: targetFile.slice(8) // отрезаем 'storage/'
    });
  });

}

/* создаем персональное хранилище */
function createPersonalStorage(storagePath, cb) {
  fs.stat(storagePath, (err, stats) => {
    if ((err && err.code === 'ENOENT') || !stats.isDirectory()) {
      fs.mkdir(storagePath, (err) => { // персональная папка
        if (err) return cb(err);
        fs.mkdir(path.join(storagePath, 'private'), (err) => { // папка для приватных файлов
          if (err) return cb(err);
          fs.mkdir(path.join(storagePath, 'public'), (err) => { // папка для публичных файлов
            if (err) return cb(err);
            return cb(null);
          });
        });
      });
    } else if (err) {
      return cb(err);
    } else {
      return cb(null);
    }    
  })
}

/* фильтруем полученные файлы */
function fileFilter(files) {
  if (!files.length) return [];
  let
    allowExt = ['jpeg', 'jpg', 'png', 'gif'],
    fileSize = cfg.get('upload.fileSize'),
    goodFiles = [];

  files.forEach((file) => {
    let currExt = file.originalname.split('.').pop().toLowerCase();
    if (file.size <= fileSize && ~allowExt.indexOf(currExt)) {
      goodFiles.push(file);
    } else {
      console.log('Недопустимое расширение файла');
      fs.unlink(file.path);
    }
  });
  
  return goodFiles;  
}

/* удаление одного файла */
function deleteImg(req, res, next) {
  let
    src = req.body.path,
    id = src.split('/').shift(),
    img = path.join('./storage', src);

  console.log(src, id, req.user._id);

  if (!src) return next(new HttpError(400));
  if (id !== String(req.user._id)) return next(new HttpError(403));

  fs.unlink(img, (err) => {
    if (err) return next(err);
    res.send()
  });

}

/* изменить доступ к файлу */
function toggleAccess(req, res, next) {
  let
    src = req.body.path,
    id = src.split('/').shift(),
    name = src.split('/').pop(),
    storage = path.join('./storage', id),
    privateDir = path.join(storage, 'private'),
    publicDir = path.join(storage, 'public'),
    oldPath, newPath;

  if (!src) return next(new HttpError(400));
  if (id !== String(req.user._id)) return next(new HttpError(403));

  fs.stat(storage, (err, stats) => {
    if ((err && err.code === 'ENOENT') || !stats.isDirectory()) return next(new HttpError(400));
    else if (err) return next(err);
    else {
      fs.readdir(privateDir, (err, list) => {
        if (err) return next(err);
        if (~list.indexOf(name)) {
          oldPath = path.join(privateDir, name);
          newPath = path.join(publicDir, name);
          moveFile(oldPath, newPath, (err) => {
            if (err) return next(err);
              res.json({
                path: newPath.slice(8) // отрезаем 'storage/'
              });
          });
        } else {
          fs.readdir(publicDir, (err, list) => {
            if (err) return next(err);
            if (~list.indexOf(name)) {
              oldPath = path.join(publicDir, name);
              newPath = path.join(privateDir, name);
              moveFile(oldPath, newPath, (err) => {
                if (err) return next(err);
                res.json({
                  path: newPath.slice(8) // отрезаем 'storage/'
                });
              });
            } else return next(new HttpError(404, 'File not found'));
          })
        }
      })
    }
  });
}

/* переместить файл */
function moveFile(oldPath, newPath, cb) {
  let
    readStream = fs.createReadStream(oldPath),
    writeStream = fs.createWriteStream(newPath, {flags: 'wx'});

  readStream
    .pipe(writeStream)
    .on('error', err => {
      console.error(err);
      if (err.code == 'EEXIST') { // файл уже существует
        fs.unlink(oldPath, (err) => {
          if (err) return cb(err);
          return cb(new HttpError(409));
        });
      }
      return cb(err);
    })
    .on('close', () => {
      fs.unlink(oldPath, (err) => {
        if (err) return cb(err);
        return cb(null);
      });
    })
}