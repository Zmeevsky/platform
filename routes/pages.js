'use strict';

const
  mongoose = require('mongoose'),
  HttpError = require('lib/HttpError'),
  cookie = require('lib/cookie');

module.exports = {
  index: index,
  login: login,
  join: join,
  chat: chat,
  gallery: gallery,
  profile: profile
};

function index(req, res, next) {
  res.render('index', {user: req.user});
}

function login(req, res, next) {
  res.render('login', {user: req.user});
}

function join(req, res, next) {
  res.render('join', {user: req.user});
}

function chat(req, res, next) {
  let Room = mongoose.model('Room');
  Room.findOne({type: 0}, (err, room) => {
    if (err) return next(err);
    if (!room) {
      let mainRoom = new Room({
        type: 0,
        name: 'Main room'
      });

      mainRoom.save((err, room) => {
        if (err) return next(err);
        cookie.set(req, res, 'pl-room', room._id);
        res.render('chat', {user: req.user});
      });
    } else {
      cookie.set(req, res, 'pl-room', room._id);
      res.render('chat', {user: req.user});
    }
  });
}

function gallery(req, res, next) {
  res.render('gallery', {user: req.user});
}

function profile(req, res, next) {
    res.render('profile', {user: req.user});
}