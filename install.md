### Install this packages:
* **MongoDB:** [mongodb.com](https://www.mongodb.com)
* **Redis:** [redis.io](http://redis.io/)
* **PM2:** [pm2.keymetrics.io](http://pm2.keymetrics.io/)
* **Nodemon:** [nodemon.io](https://nodemon.io/)

### Project launch:
* in cluster mode: 
```
pm2 start launch.json
```
* simple:
```
npm start
```