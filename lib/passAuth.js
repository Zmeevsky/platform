'use strict';
const mongoose = require('mongoose');

module.exports = {
  serialize: serialize,
  deserialize: deserialize,
  checkUserData: checkUserData
};

function serialize(user, done) {
  //console.log('serialize:', user);
  done(null, user.email);
}

function deserialize(email, done) {
  let User = mongoose.model('User');
  User.findOne({email: email}, (err, user) => {
    //console.log('deserialize:', err, user);
    done(err, user);
  })
}

function checkUserData(email, password, done) {
  let
    User = mongoose.model('User'),
    msg = { message: 'Неправильный email или пароль' };

  User.findOne({email: email}, (err, user) => {
    //console.log('checkUserData:', err, user);
    if (err) return done(err);
    if (!user) return done(null, false, msg);
    if (!user.checkPassword(password)) return done(null, false, msg);
    return done(null, user);
  })
}

