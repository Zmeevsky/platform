'use strict';

module.exports = {
  set: set,
  delAll: delAll
};

function set(req, res, name, value, options) {
  console.log('set cookie: ', name, value, options);
  let cookie = req.cookies[name];
  options = options || {};
  if (!cookie || cookie == undefined) {
      res.cookie(name, value, options);
      console.log('req.cookies ', req.cookies);
  }
}

function delAll(req, res) {
  for (let key in req.cookies) {
    if(req.cookies.hasOwnProperty(key)) {
      res.clearCookie(key);
    }
  }
}