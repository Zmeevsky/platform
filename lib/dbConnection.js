'use strict';
const
  mongoose = require('mongoose'),
  cfg = require('config');

module.exports = dbConnection;

function dbConnection() {
  mongoose.connect(cfg.get('db.address'), cfg.get('db.options'));
  console.log('[MONGOOSE] DB connected!');
  //mongoose.set('debug', true); // трансляция запросов мангуса к монге  
}

mongoose.connection.on('disconnected', () => {
  dbConnection();
});

mongoose.connection.on('error', error => {
  console.error('[MONGOOSE ERROR]', error);
});
