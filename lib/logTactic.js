'use strict';
const
  fs = require('fs'),
  util = require('util');

module.exports = (isDev) => {
  let logStream, errStream, msg;
  
  if (isDev) {

    logStream = process.stdout;
    errStream = process.stderr;

    console.log = function() {
      msg = [`[${new Date().toLocaleString()}] ${util.format.apply(this, arguments)}`, `\n`].join();
      logStream.write(msg)
    };

    console.error = function() {
      msg = [`[${new Date().toLocaleString()}] ${util.format.apply(this, arguments)}`, `\n`].join();
      errStream.write(msg);
    };

  } else {
    
    let dateStr = new Date().toLocaleString();
    
    logStream = fs.createWriteStream('./node_logs/' + dateStr + '_log.log', {'flags': 'a'});
    errStream = fs.createWriteStream('./node_logs/' + dateStr + '_err.log', {'flags': 'a'});
    
    console.log = function() {
      msg = [`[${new Date().toLocaleString()}] ${util.format.apply(this, arguments)}`, `\n`].join();
      logStream.write(msg);
      process.stdout.write(msg);
    };
    
    console.error = function() {
      msg = [`[${new Date().toLocaleString()}] ${util.format.apply(this, arguments)}`, `\n`].join();
      errStream.write(msg);
      process.stdout.write(msg);
    };
    
  }

};