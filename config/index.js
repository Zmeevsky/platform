'use strict';
module.exports = {
  get: get,
  has: has
};

let config =
  {
    app: {
      port: 3333
    },
    db: {
      address: "mongodb://localhost/zzz",
      options: {
        server: {
          socketOptions: {
            keepAlive: 1
          },
          poolSize: 5
        }
      }
    },
    hash: {
      length: 128,
      iterations: 10000
    },
    session: {
      secret: 'kill_all_humans',
      name: 'pl-sid',
      saveUninitialized: false,
      resave: false,
      cookie: {
        path: '/',
        httpOnly: true,
        maxAge: 7 * 24 * 60 * 60 * 1000
      }
    },
    redis: {
      host: 'localhost',
      port: 6379
    },
    upload: {
      fileSize: 5 * 1024 * 1024,
      storageSize: 100 * 1024 * 1024
    }
  };

function getValue(object, property) {
  let elems = Array.isArray(property) ? property : property.split('.');
  let name = elems[0];
  let value = object[name];

  if (elems.length <= 1) {
    return value;
  }
  // Note that typeof null === 'object'
  if (value === null || typeof value !== 'object') {
    return undefined;
  }
  return getValue(value, elems.slice(1));
}

function get(property) {
  return getValue(config, property);
}

function has(property) {
  if (property === null || property === undefined) {
    return false;
  }
  return getValue(config, property) !== undefined;
}
