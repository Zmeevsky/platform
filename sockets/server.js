'use strict';

module.exports = function(io) {
  io.on('connection', function (socket) {
    console.log('[SOCKET CONNECT IS SUCCESS]');
    require('./chat.js')(socket);

    socket.on('error', function(err) {
      console.error('SOCKET ERROR:\n', err);
      throw new Error(err);
    });
  });
};