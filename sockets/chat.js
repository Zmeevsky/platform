'use strict';
const
  mongoose = require('mongoose'),
  cookieParser = require('cookie-parser');

module.exports = function (socket) {
  var user = null, room = null;

  socket.on('join', (data) => {
    console.log('socket join', data);
    if (!data.room || !data.user) {
      return socket.emit('notify', {
        type: 'error',
        msg: 'Ошибка соединения.<br>Включите куки и перезагрузите страницу.'
      });
    }

    console.log(user, room);

    room = cookieParser.JSONCookie(data.room);
    console.log('parsed room:', room);
    socket.join(room);

    if (!user) {
      let id = cookieParser.JSONCookie(data.user);
      console.log('parsed id:', id);
      getUsername(id, (err, username) => {
        if (err) return socket.emit('error', err);
        user = username;
        socket.emit('chat_notify', 'Добро пожаловать в чат, ' + user);
        socket.to(room).emit('chat_notify', user + ' присоединился к чату.');
      });
      return;
    }
    socket.emit('chat_notify', 'Добро пожаловать в чат, ' + user);
    socket.to(room).emit('chat_notify', user + ' присоединился к чату.');
  });

  socket.on('disconnect', (data) => {
    socket.to(room).emit('chat_notify', user + ' покинул чат.');
  });

  socket.on('message', (message) => {
    let msg = String(message);
    if (msg) {
      socket.emit('message', {
        user: user,
        msg: msg
      });
      socket.to(room).emit('message', {
        user: user,
        msg: msg,
        incoming: true
      });
    }
  });
};

function getUsername(id, cb) {
  let User = mongoose.model('User');
  User.findById(id, (err, user) => {
    if (err) return cb(err);
    if (!user) return cb(new Error('User not found'));
    console.log(user);
    console.log(user.getFullName());
    cb(null, user.getFullName());
  });
}