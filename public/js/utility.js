;(function(){

  /* поиск родителя элемента по классу */
  HTMLElement.prototype.parent = function(value) {
    var elem = this;
    while (true) {
      elem = elem.parentElement;
      if (elem === null) return null;
      if (elem.className.split(/\s+/).indexOf(value) > -1) return elem;
    }
  };

  /* удаление элемента */
  HTMLElement.prototype.remove = function() {
    var parent = this.parentElement;
    if (parent === null) return;
    parent.removeChild(this);
  };

  /* вставка перед элементом */
  HTMLElement.prototype.insertAfter = function(elem) {
    var
      parent = this.parentElement,
      next = this.nextElementSibling;
    return next ? parent.insertBefore(elem, next) : parent.appendChild(elem);
  };

})();