var Notify = undefined;

(function (Notify) {
  var error, info, success, warning, container;

  info = function (message, title, options) {
    return notify("info", message, title, "fa fa-info-circle", options);
  };

  warning = function (message, title, options) {
    return notify("warning", message, title, "fa fa-exclamation-circle", options);
  };

  error = function (message, title, options) {
    return notify("error", message, title, "fa fa-times-circle", options);
  };

  success = function (message, title, options) {
    return notify("success", message, title, "fa fa-check-circle", options);
  };

  function notify(type, message, title, icon, options) {
    var alertElem, messageElem, titleElem, iconElem, innerElem, container;

    if (typeof options === "undefined") {
      options = {};
    }

    options = {
      width: options.width || '',
      icon: options.icon || '',
      time: (Boolean(Number(options.time)) || options.time === 0) ? options.time : 3000
    };

    if (!container) {
      container = document.querySelector('#notifications');
      if (!container) {
        container = document.createElement('ul');
        container.id = 'notifications';
        document.body.appendChild(container);
      }
    }

    if (options.width) {
      container.style.width = options.width;
    }

    alertElem = document.createElement('li');
    alertElem.classList.add('notification');
    alertElem.classList.add('notification-' + type);

    setTimeout(function () {
      alertElem.classList.add('notification-open');
    }, 1);

    if (icon) {
      iconElem = document.createElement('i');
      icon = icon.trim().split(' ');
      for (var i = 0; i < icon.length; i++) {
        iconElem.classList.add(icon[i]);
      }
      alertElem.appendChild(iconElem);
    }

    innerElem = document.createElement('div');
    innerElem.classList.add('notification-block');
    alertElem.appendChild(innerElem);

    if (title) {
      titleElem = document.createElement('div');
      titleElem.classList.add('notification-title');
      titleElem.innerHTML = title;
      innerElem.appendChild(titleElem);
    }

    if (message) {
      messageElem = document.createElement('div');
      messageElem.classList.add('notification-message');
      messageElem.innerHTML = message;
      innerElem.appendChild(messageElem);
    }

    if (options.time > 0) {
      setTimeout((function () {
        leave();
      }), options.time);
    } else {
      innerElem.innerHTML += '<em>Кликните, чтобы закрыть</em>';
    }

    alertElem.onclick = leave;

    function leave() {
      if (alertElem) {
        alertElem.classList.remove('notification-open');
        alertElem.addEventListener('webkitTransitionEnd', function () {
          if (alertElem && alertElem.parentNode) {
            alertElem.parentNode.removeChild(alertElem);
          }
        });
        alertElem.addEventListener('otransitionend', function () {
          if (alertElem && alertElem.parentNode) {
            alertElem.parentNode.removeChild(alertElem);
          }
        });
        alertElem.addEventListener('oTransitionEnd', function () {
          if (alertElem && alertElem.parentNode) {
            alertElem.parentNode.removeChild(alertElem);
          }
        });
        alertElem.addEventListener('msTransitionEnd', function () {
          if (alertElem && alertElem.parentNode) {
            alertElem.parentNode.removeChild(alertElem);
          }
        });
        alertElem.addEventListener('transitionend', function () {
          if (alertElem && alertElem.parentNode) {
            alertElem.parentNode.removeChild(alertElem);
          }
        });
      }
    }

    return container.insertBefore(alertElem, container.firstChild);
  }

  Notify.defaults = {
    width: "",
    icon: "",
    time: 5000
  };

  Notify.info = info;
  Notify.warning =  Notify.warn = warning;
  Notify.error = Notify.err = error;
  Notify.success = success;

  return container = undefined;


})(Notify || (Notify = {}));