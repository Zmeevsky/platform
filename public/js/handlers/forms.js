;(function() {

  var
    joinFormBtn = document.querySelector('.pl-join_form-submit'),
    loginFormBtn = document.querySelector('.pl-login_form-submit'),
    profileFormBtns = document.querySelectorAll('.pl-profile_form-submit');

  if (loginFormBtn) {
    loginFormBtn.onclick = function () {
      sendData('/login', formHandler(this.form), {redirect: true, url: '/'});
    };
  }

  if (joinFormBtn) {
    joinFormBtn.onclick = function () {
      sendData('/join', formHandler(this.form), {redirect: true, url: '/'});
    };
  }

  if (profileFormBtns.length) {
    for(var i = 0; i < profileFormBtns.length; i++) {
      profileFormBtns[i].onclick = function () {
        sendData('/update-user-data', formHandler(this.form), {
          notify: true,
          type: 'success',
          msg: 'Данные успешно сохранены'
        });
      };
    }
  }

  function formHandler(form) {
    var
      inputs = form.elements,
      firstName = inputs.first_name,
      lastName  = inputs.last_name,
      email = inputs.email,
      password = inputs.password,
      retypePassword = inputs.retype_password,
      data = {}, i = 0;

    form.onsubmit = function(e) { e.preventDefault(); };

    if(firstName && !nameCheck(firstName.value)) {
      Notify.warn('Имя должно начинаться с буквы<br>Имя может содержать буквы и символ "-" ');
      return false;
    }
    if(lastName && !nameCheck(lastName.value)) {
      Notify.warn('Фамилия должно начинаться с буквы<br>Фамилия может содержать буквы и символ "-" ');
      return false;
    }
    if(email && !emailCheck(email.value)) {
      Notify.warn('Некорректный email');
      return false;
    }
    if(password && !pswdCheck(password.value)) {
      Notify.warn('Длина пароля от 4 до 40 символов<br><b>Запрещенные символы:</b><b><code>  "  \'  `</code></b>');
      return false;
    }
    if(retypePassword && retypePassword.value !== password.value) {
      Notify.warn('Пароли не совпадают');
      return false;
    }

    for (i; i < inputs.length; i++) {
      if (inputs[i].name && inputs[i].value) {
        data[inputs[i].name] = inputs[i].value;
      }
    }

    return data;
  }
  
  function nameCheck(value) {
    var re = /^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z-]{1,30}$/;
    return re.test(value);
  }

  function emailCheck(value) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
  }

  function pswdCheck(value) {
    var re = /^[^"'`]{4,40}$/;
    return re.test(value);
  }

  function sendData(url, data, directive) {
    if(!url || !data) return false;
    console.log(url, data);
    $.ajax({
      type: 'POST',
      url: url,
      data: data,
      success: function (res, status, xhr) {
        console.log(status, res);

        if(directive.redirect) {
          window.location.replace(directive.url);
        }

        if(directive.notify) {
          Notify[directive.type](directive.msg);
        }

      },
      error: function (res, status, xhr) {
        var json = res.responseJSON, i = 0;
        console.error(status, res);
        if (Array.isArray(json)) {
          for (i; i < json.length; i++) {
            Notify.error(json[i].message, '', {time: 0});
          }
          return;
        }
        Notify.error(json.message, '', {time: 0});
      }
    });
  }

})();