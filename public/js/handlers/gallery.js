;(function() {
  document.addEventListener("DOMContentLoaded", getAllImgs);

  var zone = document.body;

  zone.ondragenter = function (e) {
    e.stopPropagation();
    e.preventDefault();
    showDropZone();
  };

  zone.ondragover = function (e) {
    e.stopPropagation();
    e.preventDefault();
  };

  zone.ondragleave = function (e) {
    e.stopPropagation();
    e.preventDefault();
  };

  zone.ondrop = function (e) {
    e.stopPropagation();
    e.preventDefault();
  };

  function showDropZone() {
    var dz = document.querySelector('.dropzone-wrap'),
      border = document.querySelector('.dropzone-border'),
      text = document.querySelector('.dropzone-text');

    if (!text) {
      text = document.createElement('div');
      text.classList.add('dropzone-text');
      text.innerHTML = '<span>Загрузить изображения<br> Размер не должен превышать 5 Мб</span>';
      document.body.appendChild(text);
    }

    if (!border) {
      border = document.createElement('div');
      border.classList.add('dropzone-border');
      document.body.appendChild(border);
    }

    if (!dz) {
      dz = document.createElement('div');
      dz.classList.add('dropzone-wrap');
      document.body.appendChild(dz);

      dz.ondragenter = function (e) {
        e.stopPropagation();
        e.preventDefault();
      };

      dz.ondragover = function (e) {
        e.stopPropagation();
        e.preventDefault();
      };

      dz.ondragleave = function (e) {
        e.stopPropagation();
        e.preventDefault();
        delDropZone();
      };

      dz.ondrop = function (e) {
        e.stopPropagation();
        e.preventDefault();
        doDrop(e);
        delDropZone();
      };
    }
  }

  function delDropZone() {
    var dz = document.querySelector('.dropzone-wrap'),
      border = document.querySelector('.dropzone-border'),
      text = document.querySelector('.dropzone-text');

    if (dz) {
      dz.parentNode.removeChild(dz);
    }

    if (text) {
      text.parentNode.removeChild(text);
    }

    if (border) {
      border.parentNode.removeChild(border);
    }
  }

  function doDrop(e) {
    var files = e.dataTransfer.files,
      data = new FormData(),
      result = fileFilter(files);

    for (var key in result.good) {
      if (result.good.hasOwnProperty(key)) {
        data.append(key, result.good[key]);
      }
    }

    result.bad.forEach(function (file) {
      Notify.warning(file.file.name + ':<br>' + file.error);
    });

    if (!result.good.length) return false;

    uploader(data);
  }

  function fileFilter(files) {
    var good = [], bad = [];
    [].forEach.call(files, function (file) {
      var
        allowExt = ['jpeg', 'jpg', 'png', 'gif'],
        currExt = file.name.split('.').pop().toLowerCase();

      if (allowExt.indexOf(currExt) == -1) {
        bad.push({
          error: 'Недопустимое расширение файла',
          file: file
        });
        return false;
      }

      if (file.size > 5 * 1024 * 1024) {
        bad.push({
          error: 'Размер файла больше 5 Мбайт',
          file: file
        });
        return false;
      }

      good.push(file);
    });

    return {
      good: good,
      bad: bad
    }
  }

  function uploader(files) {
    $.ajax({
      type: 'POST',
      url: '/uploads/multi',
      data: files,
      cache: false,
      processData: false, // Не обрабатываем файлы
      contentType: false, // Так jQuery скажет серверу что это строковой запрос
      success: function (res, status, xhr) {
        console.log(status, res);
        drawNewImgs(res, true);
      },
      error: function (res, status, xhr) {
        console.error(status, res);
        Notify.error(res.status +'<br>'+ res.statusText, '', {time: 0});
      }
    });
  }

  function drawNewImgs(array, isNew) {
    var zone = document.querySelector('.pl-images-container');
    if(array.length) {
      array.forEach(function (file) {
        var div = document.createElement('div');
        div.classList.add('pl-image-block');
        div.innerHTML =
          '<div class="pl-thumbnail user-select-none">' +
          '<span class="pl-image-helper"></span>' +
          '<a class="pl-image-wrap" href="'+ file.path +'" data-caption="'+ file.originalName +'">' +
          '<img src="'+ file.path +'" >' +
          '</a>' +
          '<div class="pl-image-control">' +
          '<a class="pl-image-link-wrap" data-href="'+ file.path +'" target="_blank"><i class="material-icons pl-image-link">open_in_new</i></a>' +
          '<div class="switch pl-public-toggle-wrap">'+
          '<label>'+
          '<input class="pl-public-toggle" type="checkbox" '+ ((file.path.indexOf('public') > -1) ? "checked" : "") +'>'+
          '<span class="lever"></span>'+
          '</label>'+
          '</div>' +
          '<a class="pl-image-del user-select-none">×</a>' +
          '</div>' +          
          '<div class="pl-image-name">'+ file.originalName +'</div>' +
          '</div>';

        if (isNew) {
          zone.insertBefore(div, zone.firstElementChild);
        } else {
          zone.appendChild(div);
        }

        div.querySelector('.pl-image-del').onclick = function() {
          deleteImg(this);
        };

        div.querySelector('.pl-public-toggle').onclick = function() {
          toggleAccess(this);
        };

        div.querySelector('.pl-image-link-wrap').onclick = function() {
          openLink(this);
        };

      });
      initLightbox('.pl-images-container');
    }
  }
  
  function getAllImgs() {
    $.ajax({
      type: 'GET',
      url: '/gallery/get-all',
      success: function (res, status, xhr) {
        if(!res.length) return false;

        var sorted = res.sort(function (a, b) {
          return a.created > b.created;
        });
        drawNewImgs(sorted, true);
      },
      error: function (res, status, xhr) {
        console.error(status, res);
        Notify.error(res.status +'<br>'+ res.statusText, '', {time: 0});
      }
    });
  }

  function deleteImg(self) {
    var
      parent = self.parent('pl-image-block'),
      img = parent.querySelector('img'),
      src = img.getAttribute('src');

    if (!src) return false;

    $.ajax({
      type: 'POST',
      url: '/gallery/img-delete',
      data: {path: src},
      success: function (res, status, xhr) {
        parent.remove();
        initLightbox('.pl-images-container');
      },
      error: function (res, status, xhr) {
        console.error(status, res);
        Notify.error(res.status +'<br>'+ res.statusText, '', {time: 0});
      }
    });
  }

  function toggleAccess(self) {
    var
      parent = self.parent('pl-image-block'),
      img = parent.querySelector('img'),
      src = img.getAttribute('src'),
      link = parent.querySelector('.pl-image-link-wrap'),
      a = parent.querySelector('.pl-image-wrap');

    $.ajax({
      type: 'POST',
      url: '/gallery/toggle-access',
      data: {path: src},
      success: function (res, status, xhr) {
        link.setAttribute('data-href', res.path);
        a.setAttribute('href', res.path);
        initLightbox('.pl-images-container');
      },
      error: function (res, status, xhr) {
        console.error(status, res);
        Notify.error(res.status +'<br>'+ res.statusText, '', {time: 0});
      }
    });
  }

  function openLink(self) {
    var win = window.open(self.getAttribute('data-href'), '_blank');
    win.focus();
  }

  function initLightbox(selector) {
    baguetteBox.run(selector, {
      noScrollbars: true
    })
  }

})();