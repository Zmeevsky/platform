;(function () {
  document.addEventListener('DOMContentLoaded', function () {
    var cookieMsg = 'Для работы чата необходимо включить куки и обновить страницу';
    if (!checkCookie()) {
      return Notify.warn(cookieMsg, '', {time: 0});
    }

    var host = location.host;
    var socket = io.connect(host);
    var chat = document.querySelector('.pl-chat');
    var zone = document.querySelector('.pl-msg-zone');
    var send = document.querySelector('.pl-msg-send');

    socket.on('connect', function() {
      if (!checkCookie()) {
        return Notify.warn(cookieMsg, '', {time: 0});
      }
      socket.emit('join', {
        room: getCookie('pl-room'),
        user: getCookie('pl-uid')
      });
    });

    socket.on('chat_notify', function(msg) {
      chatNotify(msg);
    });

    socket.on('message', function(data) {
      var msg = createMsg(data.user, data.msg, data.incoming);
      chat.appendChild(msg);
      chat.scrollTop = chat.scrollHeight;
    });



    function sendMsg() {
      if (!checkCookie()) {
        return Notify.warn(cookieMsg, '', {time: 0});
      }
      if (!zone || !chat || !zone.value.replace(/\r\n+|\r+|\n+/g,'')) return false;

      var text = zone.value.replace(/\r\n+|\r+|\n+/g,'<br/>');
      socket.emit('message', text);
      zone.value = '';
      zone.focus();
    }

    function createMsg(from, text, incoming) {
      var time = (new Date()).toLocaleTimeString();
      var msg = document.createElement('div');
      msg.classList.add('pl-msg-wrap');
      msg.innerHTML =
        '<div class="pl-msg card-panel blue lighten-'+ (incoming ? '5 left' : '4 right') + '">' +
        '<div class="pl-msg-title">' +
        '<sup class="pl-msg-time blue-grey-text text-darken-3">'+ time +'</sup>' +
        '<sup class="pl-msg-from blue-grey-text text-darken-4">'+ from +'</sup>' +
        '</div>' +
        '<div class="pl-chat-text">' + text + '</div>' +
        '</div>' +
        '<br class="clear">';

      return msg;
    }

    function checkCookie() {
      return navigator.cookieEnabled;
    }
    
    function chatNotify(msg) {
      if (!chat || !msg) return false;
      var notify = document.createElement('div');
      notify.classList.add('pl-chat-notify');
      notify.classList.add('card-panel');
      notify.classList.add('center-align');
      notify.classList.add('blue');
      notify.classList.add('lighten-5');
      notify.innerText = msg;
      chat.appendChild(notify);
    }


    send.onclick = sendMsg;
    zone.onkeyup = function (event) { // отправка по shift+enter
      if(event.shiftKey) {
        if (event.keyCode == 13) {
          sendMsg();
        }
      }
    }
  })
})();